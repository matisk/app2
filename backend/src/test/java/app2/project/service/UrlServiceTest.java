package app2.project.service;

import app2.project.model.Url;
import app2.project.repo.UrlRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import static org.junit.jupiter.api.Assertions.*;

class UrlServiceTest {

    @Autowired
    private UrlService underTest;

    @Autowired
    private UrlRepo urlRepo;

    @Test
    void addUrl() {
        Url url = new Url(
                "category",
                "url",
                "icon",
                "title",
                "subtitle",
                "text",
                "imageUrl"
        );
        underTest.addUrl(url);
        verify(urlRepo, atLeastOnce()).save(url);
    }

    @Test
    void findAllUrls() {
    }

    @Test
    void updateUrl() {
    }

    @Test
    void findUrlById() {
    }

    @Test
    void deleteUrl() {
    }
}