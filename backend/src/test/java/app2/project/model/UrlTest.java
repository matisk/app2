package app2.project.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class UrlTest {

    @Test
    void shouldOverrideToString() {
        //given
        Url url = new Url(
                "category",
                "url",
                "icon",
                "title",
                "subtitle",
                "text",
                "imageUrl"
        );
        //when
        String expected = "Url{" +
                "id=" + "null" +
                ", category='" + "category" + '\'' +
                ", url='" + "url" + '\'' +
                ", icon='" + "icon" + '\'' +
                ", title='" + "title" + '\'' +
                ", subtitle='" + "subtitle" + '\'' +
                ", text='" + "text" + '\'' +
                ", imageUrl='" + "imageUrl" + '\'' +
                '}';
        //then
        assertEquals(expected, url.toString());
    }
}