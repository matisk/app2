package app2.project.resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import app2.project.model.Url;
import app2.project.service.UrlService;

import java.util.List;

@RestController
@RequestMapping("/url")
public class UrlResource {
    private final UrlService urlService;

    public UrlResource(UrlService urlService) {
        this.urlService = urlService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Url>> getAllUrls () {
        List<Url> urls = urlService.findAllUrls();
        return new ResponseEntity<>(urls, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Url> getUrlById (@PathVariable("id") Long id) {
        Url url = urlService.findUrlById(id);
        return new ResponseEntity<>(url, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Url> addUrl(@RequestBody Url url) {
        Url newUrl = urlService.addUrl(url);
        return new ResponseEntity<>(newUrl, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Url> updateUrl(@RequestBody Url url) {
        Url updateUrl = urlService.updateUrl(url);
        return new ResponseEntity<>(updateUrl, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUrl(@PathVariable("id") Long id) {
        urlService.deleteUrl(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
