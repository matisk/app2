package app2.project.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import app2.project.model.Url;

import java.util.Optional;

public interface UrlRepo extends JpaRepository<Url, Long> {
    void deleteUrlById(Long id);
    
    Optional<Url> findUrlById(Long id);
}