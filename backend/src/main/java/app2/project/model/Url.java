package app2.project.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Url implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false, unique = true)
    private Long id;
    private String category;
    @Column(length = 20, nullable = false, unique = true)
    private String url;
    @Column(length = 20)
    private String icon;
    @Column(length = 50)
    private String title;
    @Column(length = 200)
    private String subtitle;
    @Column(length = 5000)
    private String text;
    @Column(length = 2000)
    private String imageUrl;

    public Url() {}

    public Url(String category, String url, String icon, String title, String subtitle, String text, String imageUrl) {
        this.category = category;
        this.url = url;
        this.icon = icon;
        this.title = title;
        this.subtitle = subtitle;
        this.text = text;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUrl() {
        return url;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Url{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", url='" + url + '\'' +
                ", icon='" + icon + '\'' +
                ", title='" + title + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", text='" + text + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
