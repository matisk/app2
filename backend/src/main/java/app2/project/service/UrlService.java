package app2.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app2.project.model.Url;
import app2.project.repo.UrlRepo;
import app2.project.exception.UrlNotFoundException;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UrlService {

    private final UrlRepo urlRepo;

    @Autowired
    public UrlService(UrlRepo urlRepo) {
        this.urlRepo = urlRepo;
    }

    public Url addUrl(Url url) { return urlRepo.save(url); }

    public List<Url> findAllUrls() {
        return urlRepo.findAll();
    }

    public Url updateUrl(Url url) {
        return urlRepo.save(url);
    }

    public Url findUrlById(Long id) {
        return urlRepo.findUrlById(id)
                .orElseThrow(() -> new UrlNotFoundException("Url by id " + id + " was not found"));
    }

    public void deleteUrl(Long id){
        urlRepo.deleteUrlById(id);
    }
    
}
