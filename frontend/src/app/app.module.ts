import { InjectionToken, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormComponent } from './components/form/form.component';
import { EditListComponent } from './components/edit-list/edit-list.component';
import { SidenavItemComponent } from './components/sidenav/sidenav-item/sidenav-item.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { FirstToUpperPipe } from './components/pipelines/first-to-upper.pipe';
import { DotdotdotPipe } from './components/pipelines/dotdotdot.pipe';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { EditListItemComponent } from './components/edit-list/edit-list-item/edit-list-item.component';
import { PageComponent } from './components/page/page.component';
import { EditDialogComponent } from './components/edit-list/edit-dialog/edit-dialog.component';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { RemoveItemComponent } from './components/edit-list/remove-item/remove-item.component';
import { MatBottomSheet } from '@angular/material/bottom-sheet';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    ToolbarComponent,
    FormComponent,
    EditListComponent,
    SidenavItemComponent,
    HomePageComponent,
    FirstToUpperPipe,
    DotdotdotPipe,
    EditListItemComponent,
    PageComponent,
    EditDialogComponent,
    RemoveItemComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatSelectModule,
    FormsModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatDialogModule
  ],
  providers: [
    HttpClient,
    MatSnackBar,
    MatDialog,
    MatBottomSheet
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
