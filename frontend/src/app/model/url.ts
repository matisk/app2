export interface Url {
    id: number,
    category: string,
    url: string,
    icon: string,
    title: string,
    subtitle: string,
    text: string,
    imageUrl: string
}
