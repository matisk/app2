import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './components/form/form.component';
import { EditListComponent } from './components/edit-list/edit-list.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { PageComponent } from './components/page/page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'form', component: FormComponent },
  { path: 'edit', component: EditListComponent },
  { path: ':url', component: PageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
