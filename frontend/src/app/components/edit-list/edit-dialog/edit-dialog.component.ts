import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Icon } from '../../../model/icon';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map, startWith } from 'rxjs/operators';
import { Url } from '../../../model/url';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UrlService } from '../../../services/url.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss']
})
export class EditDialogComponent implements OnInit {

  formControl = new FormControl();
  filteredIcons: Observable<Icon[]>;
  icons: Icon[] = [
    {
      name: 'Home',
      code: 'home',
    },
    {
      name: 'Savings',
      code: 'savings',
    },
    {
      name: 'Watch later',
      code: 'watch_later',
    },
    {
      name: 'View in AR',
      code: 'view_in_ar',
    },
    {
      name: 'Group work',
      code: 'group_work',
    },
    {
      name: 'Build circle',
      code: 'build_circle',
    },
    {
      name: 'Thumbs up down',
      code: 'thumbs_up_down',
    },
    {
      name: 'Invert colors',
      code: 'invert_colors',
    },
    {
      name: 'Mark as unread',
      code: 'mark_as_unread',
    },
    {
      name: 'Accessible forward',
      code: 'accessible_forward',
    },
    {
      name: 'Mark as unread',
      code: 'mark_as_unread',
    }
  ];
  editForm: any;

  constructor(
    private urlService: UrlService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public item: any
  ) {
    console.log(item);
    this.filteredIcons = this.formControl.valueChanges
      .pipe(
        startWith(''),
        map(icon => icon ? this._filterIcons(icon) : this.icons.slice())
      );
    this.editForm = formBuilder.group({
      id: [''],
      category: ['', [Validators.required]],
      url: ['', [Validators.maxLength(20), Validators.pattern('[a-z]*')]],
      icon: [''],
      title: ['', [Validators.required, Validators.maxLength(50)]],
      subtitle: ['', [Validators.maxLength(200)]],
      text: ['', [Validators.maxLength(5000)]],
      imageUrl: ['']
    })
  }

  ngOnInit(): void {
  }

  private _filterIcons(value: string): Icon[] {
    const filterValue = value.toLowerCase();
    return this.icons.filter(icon => icon.name.toLowerCase().indexOf(filterValue) === 0);
  }

  onSubmit() {
    console.log(this.editForm);
    this.urlService.updateUrl(this.editForm.value).subscribe(
        (response: Url) => {
          this.openConfirmationSnackBar();
        },
        (error: HttpErrorResponse) => {
          this.openErrorSnackBar(error);
          this.editForm.reset();
        }
      );
    this.dialogRef.close();
    location.reload();
  }

  openErrorSnackBar(error: any) {
    this.snackBar.open(error.message, 'Close', {
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }

  private openConfirmationSnackBar() {
    this.snackBar.open('Changes were saved', 'Close', {
      duration: 2000,
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }
}
