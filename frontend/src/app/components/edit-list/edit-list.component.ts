import { Component, OnInit } from '@angular/core';
import { Url } from '../../model/url';
import { UrlService } from '../../services/url.service';

@Component({
  selector: 'app-edit-list',
  templateUrl: './edit-list.component.html',
  styleUrls: ['./edit-list.component.scss']
})
export class EditListComponent implements OnInit {
  panelOpenState = false;

  public items: Url[] | undefined;

  constructor(
    private urlService: UrlService
  ) { }

  ngOnInit(): void {
    this.getUrls();
  }

  public getUrls(): void {
    this.urlService.getUrls().subscribe(
      (response: Url[]) => {
        this.items = response;
      }
    );
  }

}
