import { Component, Inject, OnInit } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { UrlService } from '../../../services/url.service';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-remove-item',
  templateUrl: './remove-item.component.html',
  styleUrls: ['./remove-item.component.scss']
})
export class RemoveItemComponent implements OnInit {

  constructor(
    private _bottomSheetRef: MatBottomSheetRef<RemoveItemComponent>,
    private urlService: UrlService,
    private snackBar: MatSnackBar,
    @Inject(MAT_BOTTOM_SHEET_DATA) public item: any
  ) { }

  ngOnInit(): void {
  }

  remove() {
    this.urlService.deleteUrl(this.item.item.id).subscribe(
      (response: void) => {
        this.openConfirmationSnackBar();
        this._bottomSheetRef.dismiss();
        location.reload();
      },
      (error: HttpErrorResponse) => {
        this.openErrorSnackBar(error);
        this._bottomSheetRef.dismiss();
      }
    );
  }

  close() {
    this._bottomSheetRef.dismiss();
  }

  openErrorSnackBar(error: any) {
    this.snackBar.open(error.message, 'Close', {
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }

  private openConfirmationSnackBar() {
    this.snackBar.open('Item was removed', 'Close', {
      duration: 2000,
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }
}
