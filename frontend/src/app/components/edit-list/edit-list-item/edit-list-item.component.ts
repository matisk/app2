import { Component, Input } from '@angular/core';
import { Url } from '../../../model/url';
import { MatDialog } from '@angular/material/dialog';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { RemoveItemComponent } from '../remove-item/remove-item.component';

@Component({
  selector: 'app-edit-list-item',
  templateUrl: './edit-list-item.component.html',
  styleUrls: ['./edit-list-item.component.scss']
})
export class EditListItemComponent {
  @Input() item: any | Url;

  panelOpenState = false;

  constructor(
    public dialog: MatDialog,
    private bottomSheet: MatBottomSheet
  ) {
  }

  edit() {
    this.dialog.open(EditDialogComponent, {
      width: '500px',
      data: { item: this.item }
    });
  }

  remove() {
    this.bottomSheet.open(RemoveItemComponent, {
      data: { item: this.item }
    });
  }
}
