import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UrlService } from '../../services/url.service';
import { Url } from '../../model/url';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  item: Url[] | undefined;
  url: any;

  constructor(
    private urlService: UrlService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(param => {
      this.url = param.get('url');
      this.getUrls();
    })
  }

  public getUrls(): void {
    this.urlService.getUrls().subscribe(
      (response: Url[]) => {
        this.item = response.filter(value => {
            return value.url === this.url;
          }
        )
      },
      (error: HttpErrorResponse) => {
        this.openErrorSnackBar(error);
      }
    );
  }

  openErrorSnackBar(error: any) {
    this.snackBar.open(error.message, 'Close', {
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }
}
