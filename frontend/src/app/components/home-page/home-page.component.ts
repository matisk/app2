import { Component, OnInit } from '@angular/core';
import { Url } from '../../model/url';
import { UrlService } from '../../services/url.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  public items: Url[] | undefined;
  public workItems: Url[] | undefined;
  public itemSlice = 250;

  constructor(
    private urlService: UrlService,
  ) { }

  ngOnInit(): void {
    this.getUrls();
  }

  public getUrls(): void {
    this.urlService.getUrls().subscribe(
      (response: Url[]) => {
        this.items = response;
        this.workItems = this.items;
      }
    );
  }

  filter(value: string) {
    this.workItems = this.items?.filter((item)=> {
      return item.category == value;
    });
  }

}
