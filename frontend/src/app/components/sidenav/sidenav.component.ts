import { Component, OnInit } from '@angular/core';
import { UrlService } from '../../services/url.service';
import { Url } from '../../model/url';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  private _items: Url[] | undefined;

  home = {
    url: '/',
    icon: 'home',
    title: 'home',
  };

  constructor(
    private urlService: UrlService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.getUrls();
  }

  public getUrls(): void {
    this.urlService.getUrls().subscribe(
      (response: Url[]) => {
        this.items = response;
      },
      (error: HttpErrorResponse) => {
        this.openErrorSnackBar(error);
      }
    );
  }

  get items(): Url[] | undefined {
    return this._items;
  }

  set items(value: Url[] | undefined) {
    this._items = value;
  }

  openErrorSnackBar(error: any) {
    this.snackBar.open(error.message, 'Close', {
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }
}
