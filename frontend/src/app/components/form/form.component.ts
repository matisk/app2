import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Icon } from '../../model/icon';
import { UrlService } from '../../services/url.service';
import { Url } from '../../model/url';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent {

  formControl = new FormControl();
  filteredIcons: Observable<Icon[]>;

  icons: Icon[] = [
    {
      name: 'Home',
      code: 'home',
    },
    {
      name: 'Savings',
      code: 'savings',
    },
    {
      name: 'Watch later',
      code: 'watch_later',
    },
    {
      name: 'View in AR',
      code: 'view_in_ar',
    },
    {
      name: 'Group work',
      code: 'group_work',
    },
    {
      name: 'Build circle',
      code: 'build_circle',
    },
    {
      name: 'Thumbs up down',
      code: 'thumbs_up_down',
    },
    {
      name: 'Invert colors',
      code: 'invert_colors',
    },
    {
      name: 'Mark as unread',
      code: 'mark_as_unread',
    },
    {
      name: 'Accessible forward',
      code: 'accessible_forward',
    },
    {
      name: 'Mark as unread',
      code: 'mark_as_unread',
    }
  ];
  addForm: any;

  constructor(
    private urlService: UrlService,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    this.filteredIcons = this.formControl.valueChanges
      .pipe(
        startWith(''),
        map(icon => icon ? this._filterIcons(icon) : this.icons.slice())
      );
    this.addForm = formBuilder.group({
      category: ['', [Validators.required]],
      url: ['', [Validators.maxLength(20), Validators.pattern('[a-z]*')]],
      icon: ['', ],
      title: ['', [Validators.required, Validators.maxLength(50)]],
      subtitle: ['', [Validators.maxLength(200)]],
      text: ['', [Validators.maxLength(5000)]],
      imageUrl: ['']
    })
  }

  private _filterIcons(value: string): Icon[] {
    const filterValue = value.toLowerCase();
    return this.icons.filter(icon => icon.name.toLowerCase().indexOf(filterValue) === 0);
  }

  onSubmit() {
    if (this.addForm.value.url === "") {
      this.addForm.value.url = this.addForm.value.title.replace(/ .*/,'')
    }
    this.urlService.addUrl(this.addForm.value).subscribe(
      (response: Url) => {
        this.addForm.reset();
        this.openConfirmationSnackBar();
        location.reload();
      },
      (error: HttpErrorResponse) => {
        this.openErrorSnackBar(error);
        this.addForm.reset();
      }
    );
  }

  openErrorSnackBar(error: any) {
    this.snackBar.open(error.message, 'Close', {
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }

  private openConfirmationSnackBar() {
    this.snackBar.open('Item was added', 'Close', {
      duration: 2000,
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }
}
