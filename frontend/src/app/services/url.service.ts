import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Url } from '../model/url';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient){}

  public getUrls(): Observable<Url[]> {
    return this.http.get<Url[]>(`${this.apiServerUrl}/url/all`);
  }

  public addUrl(url: Url): Observable<Url> {
    return this.http.post<Url>(`${this.apiServerUrl}/url/add`, url);
  }

  public updateUrl(url: Url): Observable<Url> {
    return this.http.put<Url>(`${this.apiServerUrl}/url/update`, url);
  }

  public deleteUrl(urlId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/url/delete/${urlId}`);
  }
}
